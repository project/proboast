<?php

namespace Drupal\proboast\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Admin form for ProBoast config.
 */
class ProBoastAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'proboast_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('proboast.settings');

    // Set setup authorization code if otherwise unset.
    if (empty($config->get('setup_authorization_code'))) {
      $authorization_code = random_int(100000, 999999);
      $config->set('setup_authorization_code', $authorization_code);
    }

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['proboast.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $activated = (empty($this->config('proboast.settings')->get('activated')))
      ? $this->t('Site is not ready to receive albums/photos <strong>(Not Connected)</strong>')
      : $this->t('Site is ready albums/photos will be received and published <strong>(Connected)</strong>');

    $form['activated'] = [
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $activated,
    ];

    if (!empty($this->config('proboast.settings')->get('last_connection_time'))) {
      $form['last_connection_time'] = [
        '#type' => 'markup',
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $this->t('Last Connection: @time', ['@time' => $this->config('proboast.settings')->get('last_connection_time')]),
      ];

    }

    if (!empty($this->config('proboast.settings')->get('setup_authorization_code'))) {
      $form['setup_authorization_code'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Setup Authorization Code (Read Only)'),
        '#disabled' => TRUE,
        '#default_value' => $this->config('proboast.settings')->get('setup_authorization_code'),
        '#description' => $this->t('Copy this number to ProBoast.com in the "Active Your Website" form.'),
      ];

    }
    else {
      $form['setup_authorization_code_message'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Setup Authorization Code'),
        '#disabled' => TRUE,
        '#default_value' => $this->t('First complete all required fields and save this form.'),
        '#description' => $this->t('This number to be entered on ProBoast.com in "Active Your Website".'),
      ];

    }

    $form['website_uuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ProBoast Website UUID'),
      '#required' => TRUE,
      '#default_value' => $this->config('proboast.settings')->get('website_uuid'),
      '#description' => $this->t('Enter the value that is displayed on your website management interface on ProBoast.com'),
    ];

    $form['salt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Push Authorization Token Salt'),
      '#required' => TRUE,
      '#default_value' => $this->config('proboast.settings')->get('salt'),
      '#description' => $this->t('Enter any combination of characters/words. Do not share this value with anyone. This salt is used in part to generate the Push Authorization Token. This value can be changed at any time.'),
    ];

    $form['wrapper_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wrapper Class'),
      '#required' => FALSE,
      '#default_value' => $this->config('proboast.settings')->get('wrapper_class'),
      '#description' => $this->t('The class to add to the container element containing the images.'),
    ];

    $form['image_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image Class'),
      '#required' => FALSE,
      '#default_value' => $this->config('proboast.settings')->get('image_class'),
      '#description' => $this->t('The class to add to the image element.'),
    ];

    // Expose the callback URLs to the user for convenience.
    $form['proboast_callback_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('ProBoast Webhooks'),
      '#description' => $this->t('Enter these callback addresses into your Twilio phone number configuration on the Twilio dashboard to allow your site to respond to incoming voice calls and SMS messages.'),
    ];

    // Initialize variables.
    $webhook_url = $GLOBALS['base_url'] . '/proboast';
    $push_authorization_token = (!empty($this->config('proboast.settings')->get('push_authorization_token')))
      ? $this->config('proboast.settings')->get('push_authorization_token')
      : $this->t("Not yet set. Please proceed with Website Setup on ProBoast.com.");

    $form['proboast_callback_container']['webhook_url'] = [
      '#type' => 'item',
      '#title' => $this->t('Webhook URL'),
      '#markup' => '<p>' . $webhook_url . '</p>',
    ];

    $form['proboast_callback_container']['push_authorization_token_markup'] = [
      '#type' => 'item',
      '#title' => $this->t('Push Authorization Token'),
      '#markup' => '<p>' . $push_authorization_token . '</p>',
    ];

    return parent::buildForm($form, $form_state);
  }

}
