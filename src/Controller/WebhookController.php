<?php

namespace Drupal\proboast\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\File\FileSystemInterface;

/**
 * Default controller for the proboast module.
 */
class WebhookController extends ControllerBase {

  /**
   * Proboast Stripe.
   *
   * @var \Drupal\proboast\Service\Command
   */
  protected $proboastCommands;

  /**
   * The Symfony Request Stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * The config factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file system.
   *
   * @var Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $proboastCommands = $container->get('proboast.command');
    $request = $container->get('request_stack');
    $configFactory = $container->get('config.factory');
    $entityTypeManager = $container->get('entity_type.manager');
    $fileSystem = $container->get('file_system');

    return new static($proboastCommands, $request, $configFactory, $entityTypeManager, $fileSystem);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($proboastCommands, $request, $configFactory, $entityTypeManager, $fileSystem) {
    $this->proboastCommands = $proboastCommands;
    $this->request = $request->getCurrentRequest();
    $this->configFactory = $configFactory->getEditable('proboast.settings');
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
  }

  /**
   * Receives a command from ProBoast.
   */
  public function webhook() {

    $logger = $this->getLogger('proboast');

    $next_push_authorization_token = $this->configFactory->get('push_authorization_token');

    $parameters = $this->request->request->all();

    $logger->notice('Webhook Parameters: @raw', [
      '@raw' => print_r($parameters, 1),
    ]);

    if (!empty($parameters)) {

      $node_storage = $this->entityTypeManager->getStorage('node');

      if ($parameters['command-type'] == 'connect-website') {
        $proboast_uuid = $parameters['uuid'];
        $code = $parameters['code'];

        $authorization_code = $this->configFactory->get('setup_authorization_code');
        $proboast_site_uuid_in_config = $this->configFactory->get('website_uuid');

        if (($authorization_code == $code) && ($proboast_site_uuid_in_config == $proboast_uuid)) {
          $push_authorization_token = $this->proboastCommands->setNextPushAuthorizationToken($authorization_code);
          $response['push-authorization-token'] = $push_authorization_token;
          $this->configFactory->set('activated', TRUE);
          $time = new DrupalDateTime();
          $time = $time->__toString();
          $this->configFactory->set('last_connection_time', $time);
          $this->configFactory->save();

        }
        else {
          throw new AccessDeniedHttpException();

        }

      }

      if (!empty($parameters['push-authorization-token']) &&
        $next_push_authorization_token == $parameters['push-authorization-token']) {

        switch ($parameters['command-type']) {

          case 'create-album':
            $title = $parameters['title'];
            $proboast_album_uuid = $parameters['album-uuid'];

            $album = $node_storage->create([
              'type' => 'proboast_album',
              'proboast_id' => $proboast_album_uuid,
              'title' => $title,
            ]);

            $album->save();
            $album_id = $album->id();
            $album_uuid = $album->uuid();

            if (!empty($proboast_album_uuid) && !empty($album_id)) {
              $response['album-id'] = $album_uuid;
            }
            else {
              $logger->error('Could not save new album @$proboast_album_uuid.', [
                '@proboast_album_uuid' => $proboast_album_uuid,
              ]);
            }

            break;

          case 'create-image':
            $proboast_album_uuid = $parameters['album-uuid'];
            $proboast_image_uuid = $parameters['image-uuid'];
            $image_url = $parameters['image-url'];
            $title = (!empty($parameters['title']))
              ? $parameters['title']
              : '';
            $alt_text = (!empty($parameters['alt-text']))
              ? $parameters['alt-text']
              : '';

            $proboast_album_query = $node_storage
              ->getQuery()
              ->condition('type', 'proboast_album')
              ->condition('proboast_id', $proboast_album_uuid);
            $proboast_album_results = $proboast_album_query->execute();

            if (!empty($proboast_album_results)) {

              $file = system_retrieve_file($image_url, NULL, TRUE, FileSystemInterface::EXISTS_REPLACE);

              if (!empty($file)) {

                $file_url = $file->createFileUrl();
                $file_url = file_create_url($file_url);

                if (strpos($file_url, 'http://') === FALSE && strpos($file_url, 'https://') === FALSE) {
                  $host = $this->request->getSchemeAndHttpHost();
                  $file_url = $host . $file_url;
                }

                // Return the first item in the array.
                $proboast_album = $node_storage->load(reset($proboast_album_results));

                $logger->notice('@data', [
                  '@data' => 'album: ' . $proboast_album->id(),
                ]);

                $proboast_album->get('proboast_photo')->appendItem([
                  'target_id' => $file->id(),
                  'alt' => $alt_text,
                  'title' => $title,
                ]);

                // Either FALSE, 1 for SAVED_NEW or 2 SAVED_UPDATED.
                $image_created_successfully = $proboast_album->save();

                if (!empty($image_created_successfully)) {
                  $response['image-url'] = $file_url;
                  $response['image-preferred-id'] = $file->id();

                }
                else {
                  $logger->error('Could not save image @proboast_image_id in album @proboast_album_uuid. An error occurred when attempting to create the file locally.', [
                    '@proboast_album_uuid' => $proboast_album_uuid,
                    '@proboast_image_uuid' => $proboast_image_uuid,
                  ]);
                }

              }
              else {
                $logger->error('Could not save image @proboast_image_id. Album "@proboast_album_uuid" does not exist.', [
                  '@proboast_album_uuid' => $proboast_album_uuid,
                  '@proboast_image_uuid' => $proboast_image_uuid,
                ]);
              }

            }
            else {
              $logger->error('Album @proboast_album_uuid does not exist', [
                '@proboast_album_uuid' => $proboast_album_uuid,
              ]);
            }

            break;

          case 'delete-image':

            // Preset $response, so that it can be overwritten.
            $response['deleted'] = FALSE;
            $proboast_album_uuid = $parameters['album-uuid'];
            $proboast_image_uuid = $parameters['image-uuid'];
            $image_preferred_id = $parameters['image-preferred-id'];

            $proboast_album_query = $node_storage
              ->getQuery()
              ->condition('type', 'proboast_album')
              ->condition('proboast_id', $proboast_album_uuid);
            $proboast_album_results = $proboast_album_query->execute();

            if (!empty($proboast_album_results)) {

              // Return the first item in the array.
              $proboast_album = $node_storage->load(reset($proboast_album_results));

              $array_of_referenced_items = $proboast_album->get('proboast_photo')->getValue();
              $index_to_remove = array_search($image_preferred_id, array_column($array_of_referenced_items, 'target_id'));
              $proboast_album->get('proboast_photo')->removeItem($index_to_remove);
              $proboast_album->save();

              if (!empty($proboast_album->save())) {
                $response['deleted'] = TRUE;
                $logger->error('Successfully deleted image on @proboast_album_uuid', [
                  '@proboast_album_uuid' => $proboast_album_uuid,
                ]);

              }

            }

            if (!$response['deleted']) {
              $logger->error('Failed attempt to delete image on @proboast_album_uuid', [
                '@proboast_album_uuid' => $proboast_album_uuid,
              ]);

            }

            break;

        }

      }

    }
    else {

      $response['no-parameters'] = TRUE;

    }

    if (empty($response)) {
      throw new AccessDeniedHttpException();
    }

    return new JsonResponse(
      $response
    );

  }

}
