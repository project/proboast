<?php

namespace Drupal\proboast\Service;

use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Service class for ProBoast API commands.
 */
class Command {

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The config factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Initialize properties.
   */
  public function __construct(LoggerChannelFactory $loggerFactory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->loggerFactory = $loggerFactory->get('proboast');
    $this->configFactory = $config_factory->getEditable('proboast.settings');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Sets the next push authorization token.
   *
   * @return string
   *   The next push authorization token.
   */
  public function setNextPushAuthorizationToken($string_to_hash) {

    if (!empty($this->configFactory->get('salt'))) {

      // Generate new Next Push Authorization Token.
      $salt = $this->configFactory->get('salt');
      $push_authorization_token = hash('sha256', $string_to_hash . $salt . time());
      $this->configFactory->set('push_authorization_token', $push_authorization_token)
        ->save();

      return $push_authorization_token;

    }
    else {

      // No salt defined.
      throw new \Exception('Salt is not set. Cannot generate Next Push Authorization Token.');

    }
  }

}
